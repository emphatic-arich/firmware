set_property CONFIG_VOLTAGE 1.8 [current_design]
set_property CFGBVS GND [current_design]

set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 66 [current_design]

set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR NO [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property BITSTREAM.CONFIG.M1PIN PULLNONE [current_design]
set_property BITSTREAM.CONFIG.M2PIN PULLNONE [current_design]
set_property BITSTREAM.CONFIG.M0PIN PULLNONE [current_design]

set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]

# set_property PACKAGE_PIN T14 [get_ports {SYS_CLK[0]}]
# create_clock -name {SYS_CLK[0]} -period 40.000 [get_ports {SYS_CLK[0]}]

# Ethernet RefClk (125MHz)
create_clock -period 8.000 -name eth_refclk [get_ports eth_clk_p]

set_clock_groups -asynchronous \
    -group [get_clocks -include_generated_clocks eth_refclk] \
    -group [get_clocks -include_generated_clocks [get_clocks -filter {name =~ *infra*/eth/phy/*/RXOUTCLK}]]\
    -group [get_clocks -include_generated_clocks [get_clocks -filter {name =~ *infra*/eth/phy/*/TXOUTCLK}]]

# set_clock_groups -asynchronous \
#     -group [get_clocks -include_generated_clocks clk40]

set_property PACKAGE_PIN B6 [get_ports eth_clk_p]
set_property PACKAGE_PIN B5 [get_ports eth_clk_n]

set_property PACKAGE_PIN D2 [get_ports eth_tx_p]
set_property PACKAGE_PIN D1 [get_ports eth_tx_n]

set_property PACKAGE_PIN C4 [get_ports eth_rx_p]
set_property PACKAGE_PIN C3 [get_ports eth_rx_n]

set_property PACKAGE_PIN D14 [get_ports eth_los]
set_property PACKAGE_PIN E16 [get_ports eth_txdis] ; # b15_l14_p

set_property PACKAGE_PIN D11 [get_ports rled] ; # b15_l6_p
set_property PACKAGE_PIN C12 [get_ports gled] ; # b15_l6_n

set_property PACKAGE_PIN K18 [get_ports led] ; # bank 14
set_property IOSTANDARD LVCMOS18 [get_ports led]

set_property PACKAGE_PIN M15  [get_ports en_gtpwr] ; # bank 14
set_property IOSTANDARD LVCMOS18 [get_ports en_gtpwr]


set_property IOSTANDARD LVCMOS33 [get_ports rled]
set_property IOSTANDARD LVCMOS33 [get_ports gled]

set_property IOSTANDARD LVCMOS33 [get_ports eth_los]
set_property IOSTANDARD LVCMOS33 [get_ports eth_txdis]
