-- right now reads at 160 Mbps


-- Prior to the actual wishbone packet, the previous data frame is e.g.
-- 0x20000FF2f, where
-- 0x-----FF-- FF is the number of requests, from 0-1023
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;

entity wishbone_fifo_reader is
  port(
    clk   : in std_logic;
    reset : in std_logic;

    -- wishbone
    ipbus_in  : in  ipb_wbus;
    ipbus_out : out ipb_rbus;

    -- fifo
    rd_en : out std_logic;
    din   : in  std_logic_vector(31 downto 0);
    valid : in  std_logic;
    empty : in  std_logic
    );

end wishbone_fifo_reader;

architecture rtl of wishbone_fifo_reader is

  signal words_todo : integer range 0 to 1023 := 0;

  signal wdata_r  : std_logic_vector (31 downto 0) := (others => '0');
  signal wdata_rr : std_logic_vector (31 downto 0) := (others => '0');


  signal strobe_r  : std_logic := '0';
  signal strobe_os : std_logic := '0';

  component ila_0
    port (
      clk    : in std_logic;
      probe0 : in std_logic_vector(31 downto 0);
      probe1 : in std_logic_vector(31 downto 0);
      probe2 : in std_logic_vector(0 downto 0);
      probe3 : in std_logic_vector(0 downto 0);
      probe4 : in std_logic_vector(31 downto 0);
      probe5 : in std_logic_vector(0 downto 0);
      probe6 : in std_logic_vector(0 downto 0);
      probe7 : in std_logic_vector(0 downto 0);
      probe8 : in std_logic_vector(3 downto 0);
      probe9 : in std_logic_vector(31 downto 0)
      );
  end component;

begin

  ila_0_1 : ila_0
    port map (
      clk                => clk,
      probe0             => din,
      probe1             => ipbus_out.ipb_rdata,
      probe2(0)          => ipbus_in.ipb_strobe,
      probe3(0)          => ipbus_out.ipb_ack,
      probe4             => ipbus_in.ipb_wdata,
      probe5(0)          => rd_en,
      probe6(0)          => valid,
      probe7(0)          => empty,
      probe8(3 downto 0) => '0' & '0' & '0' & '0',
      probe9             => din
      );


  -- some tricks to allow this to run at full-throughput,
  -- reading a FIFO word every clock tick
  --
  -- ack connects directly to the FIFO
  --

  process (clk) is
  begin
    if (rising_edge(clk)) then

      wdata_r  <= ipbus_in.ipb_wdata;
      wdata_rr <= wdata_r;

      strobe_r <= ipbus_in.ipb_strobe;

      -- see transactor_sm.vhd
      if (strobe_os = '1') then
        words_todo <= to_integer(unsigned(wdata_rr(15 downto 8)));
      elsif (words_todo > 0) then
        words_todo <= words_todo - 1;
      end if;

    end if;
  end process;

  strobe_os <= '1' when ipbus_in.ipb_strobe='1' and strobe_r='0' else '0';

  rd_en <= '1' when words_todo > 0 else '0';

  ipbus_out.ipb_rdata <= din;
  ipbus_out.ipb_ack   <= (valid or empty);

  ipbus_out.ipb_err <= '0';

end rtl;
