---------------------------------------------------------------------------------
--
--   Copyright 2017 - Rutherford Appleton Laboratory and University of Bristol
--
--   Licensed under the Apache License, Version 2.0 (the "License");
--   you may not use this file except in compliance with the License.
--   You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--   Unless required by applicable law or agreed to in writing, software
--   distributed under the License is distributed on an "AS IS" BASIS,
--   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--   See the License for the specific language governing permissions and
--   limitations under the License.
--
--                                     - - -
--
--   Additional information about ipbus-firmare and the list of ipbus-firmware
--   contacts are available at
--
--       https://ipbus.web.cern.ch/ipbus
--
---------------------------------------------------------------------------------


-- eth_7s_1000basex_gtp
--
-- Contains the instantiation of the Xilinx MAC & 1000baseX pcs/pma & GTP transceiver cores
--
-- This version is for the artix GTP transceivers, and has the GTPE2_COMMON included in this block.
-- Various PLL clock outputs are therefore provided for use by other MGTs in the same quad.
--
-- Do not change signal names in here without corresponding alteration to the timing contraints file
--
-- Dave Newbold, January 2016
-- Tweaks by AP June 2021

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

library unisim;
use unisim.VComponents.all;
use work.emac_hostbus_decl.all;

entity eth_7s_1000basex_gtp is
  port(
    gt_clkp, gt_clkn : in  std_logic;
    gt_txp, gt_txn   : out std_logic;
    gt_rxp, gt_rxn   : in  std_logic;
    sfp_los          : in  std_logic;
    clk125_out       : out std_logic;
    clk125_fr        : out std_logic;
    pllclk_out       : out std_logic;
    pllrefclk_out    : out std_logic;
    rsti             : in  std_logic;
    locked           : out std_logic;
    tx_data          : in  std_logic_vector(7 downto 0);
    tx_valid         : in  std_logic;
    tx_last          : in  std_logic;
    tx_error         : in  std_logic;
    tx_ready         : out std_logic;
    rx_data          : out std_logic_vector(7 downto 0);
    rx_valid         : out std_logic;
    rx_last          : out std_logic;
    rx_error         : out std_logic;
    hostbus_in       : in  emac_hostbus_in := ('0', "00", "0000000000", X"00000000", '0', '0', '0');
    hostbus_out      : out emac_hostbus_out
    );

end eth_7s_1000basex_gtp;

architecture rtl of eth_7s_1000basex_gtp is

  component eth_mac_1g
    --generic (
    --  DATA_WIDTH        : integer;
    --  ENABLE_PADDING    : integer;
    --  MIN_FRAME_LENGTH  : integer;
    --  TX_PTP_TS_ENABLE  : integer;
    --  TX_PTP_TS_WIDTH   : integer;
    --  TX_PTP_TAG_ENABLE : integer;
    --  TX_PTP_TAG_WIDTH  : integer;
    --  RX_PTP_TS_ENABLE  : integer;
    --  RX_PTP_TS_WIDTH   : integer;
    --  TX_USER_WIDTH     : integer;
    --  RX_USER_WIDTH     : integer
    --  );
    port (
      rx_clk : in std_logic;
      rx_rst : in std_logic;
      tx_clk : in std_logic;
      tx_rst : in std_logic;

      -- AXI input
      tx_axis_tdata  : in  std_logic_vector;
      tx_axis_tvalid : in  std_logic;
      tx_axis_tready : out std_logic;
      tx_axis_tlast  : in  std_logic;
      tx_axis_tuser  : in  std_logic_vector;

      -- AXI output
      rx_axis_tdata  : out std_logic_vector;
      rx_axis_tvalid : out std_logic;
      rx_axis_tlast  : out std_logic;
      rx_axis_tuser  : out std_logic_vector;

      -- GMII interface
      gmii_rxd   : in  std_logic_vector;
      gmii_rx_dv : in  std_logic;
      gmii_rx_er : in  std_logic;
      gmii_txd   : out std_logic_vector;
      gmii_tx_en : out std_logic;
      gmii_tx_er : out std_logic;

      -- PTP
      tx_ptp_ts            : in  std_logic_vector (95 downto 0);
      rx_ptp_ts            : in  std_logic_vector (95 downto 0);
      tx_axis_ptp_ts       : out std_logic_vector (95 downto 0);
      tx_axis_ptp_ts_tag   : out std_logic_vector (15 downto 0);
      tx_axis_ptp_ts_valid : out std_logic;

      -- Control
      rx_clk_enable : in std_logic;
      tx_clk_enable : in std_logic;
      rx_mii_select : in std_logic;
      tx_mii_select : in std_logic;

      -- Status
      tx_start_packet    : out std_logic;
      tx_error_underflow : out std_logic;
      rx_start_packet    : out std_logic;
      rx_error_bad_frame : out std_logic;
      rx_error_bad_fcs   : out std_logic;

      -- Configuration
      ifg_delay : in std_logic_vector (7 downto 0)
      );
  end component;

  component gig_eth_pcs_pma_basex_gtp
    port (
      gtrefclk_p             : in  std_logic;
      gtrefclk_n             : in  std_logic;
      gtrefclk_out           : out std_logic;
      gtrefclk_bufg_out      : out std_logic;
      txn                    : out std_logic;
      txp                    : out std_logic;
      rxn                    : in  std_logic;
      rxp                    : in  std_logic;
      independent_clock_bufg : in  std_logic;
      userclk_out            : out std_logic;
      userclk2_out           : out std_logic;
      rxuserclk_out          : out std_logic;
      rxuserclk2_out         : out std_logic;
      resetdone              : out std_logic;
      pma_reset_out          : out std_logic;
      mmcm_locked_out        : out std_logic;
      gmii_txd               : in  std_logic_vector(7 downto 0);
      gmii_tx_en             : in  std_logic;
      gmii_tx_er             : in  std_logic;
      gmii_rxd               : out std_logic_vector(7 downto 0);
      gmii_rx_dv             : out std_logic;
      gmii_rx_er             : out std_logic;
      gmii_isolate           : out std_logic;
      configuration_vector   : in  std_logic_vector(4 downto 0);
      status_vector          : out std_logic_vector(15 downto 0);
      reset                  : in  std_logic;
      signal_detect          : in  std_logic;
      gt0_pll0outclk_out     : out std_logic;
      gt0_pll0outrefclk_out  : out std_logic;
      gt0_pll1outclk_out     : out std_logic;
      gt0_pll1outrefclk_out  : out std_logic;
      gt0_pll0lock_out       : out std_logic;
      gt0_pll0refclklost_out : out std_logic
      );
  end component;

  signal gmii_txd, gmii_rxd                             : std_logic_vector(7 downto 0);
  signal gmii_tx_en, gmii_tx_er, gmii_rx_dv, gmii_rx_er : std_logic;
  signal gmii_rx_clk                                    : std_logic;
  signal sig_det, gt_clkp_i, gt_clkn_i                  : std_logic;
  signal clk125, clk_fr                                 : std_logic;
  signal rstn, phy_done, mmcm_locked, locked_int        : std_logic;
  signal dc                                             : std_logic := '0';
  signal clk_dc                                         : std_logic;

begin

  clkp_buf : IBUF                       -- required because top level IBUF insertion does not work for IBUFDS_GT buried in PCS/PMA core
    port map(
      i => gt_clkp,
      o => gt_clkp_i
      );

  clkn_buf : IBUF
    port map(
      i => gt_clkn,
      o => gt_clkn_i
      );

  clk125_fr  <= clk_fr;
  clk125_out <= clk125;

  process(clk_fr)
  begin
    if rising_edge(clk_fr) then
      locked_int <= mmcm_locked and phy_done;
    end if;
  end process;

  locked <= locked_int;
  rstn   <= not (not locked_int or rsti);

  eth_mac_1g_inst : eth_mac_1g
    --generic map (
    --  --DATA_WIDTH        => DATA_WIDTH,
    --  --ENABLE_PADDING    => ENABLE_PADDING,
    --  --MIN_FRAME_LENGTH  => MIN_FRAME_LENGTH,
    --  --TX_PTP_TS_WIDTH   => TX_PTP_TS_WIDTH,
    --  --TX_PTP_TAG_ENABLE => TX_PTP_TAG_ENABLE,
    --  --TX_PTP_TAG_WIDTH  => TX_PTP_TAG_WIDTH,
    --  --RX_PTP_TS_ENABLE  => RX_PTP_TS_ENABLE,
    --  --RX_PTP_TS_WIDTH   => RX_PTP_TS_WIDTH,
    --  --TX_USER_WIDTH     => TX_USER_WIDTH,
    --  --RX_USER_WIDTH     => RX_USER_WIDTH,
    --  TX_PTP_TS_ENABLE  => 0
    --  )
    port map (
      rx_clk => clk125,
      tx_clk => clk125,
      rx_rst => not rstn,
      tx_rst => not rstn,

      tx_axis_tdata    => tx_data,
      tx_axis_tvalid   => tx_valid,
      tx_axis_tready   => tx_ready,
      tx_axis_tlast    => tx_last,
      tx_axis_tuser(0) => tx_error,

      rx_axis_tdata    => rx_data,
      rx_axis_tvalid   => rx_valid,
      rx_axis_tlast    => rx_last,
      rx_axis_tuser(0) => rx_error,

      gmii_rxd   => gmii_rxd,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_txd   => gmii_txd,
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,

      tx_ptp_ts            => (others => '0'),
      rx_ptp_ts            => (others => '0'),
      tx_axis_ptp_ts       => open,
      tx_axis_ptp_ts_tag   => open,
      tx_axis_ptp_ts_valid => open,

      rx_clk_enable => '1',
      tx_clk_enable => '1',

      rx_mii_select => '0',
      tx_mii_select => '0',

      tx_start_packet    => open,
      tx_error_underflow => open,
      rx_start_packet    => open,
      rx_error_bad_frame => open,
      rx_error_bad_fcs   => open,
      ifg_delay          => std_logic_vector(to_unsigned(12, 8))
      );

  hostbus_out.hostrddata  <= (others => '0');
  hostbus_out.hostmiimrdy <= '0';

-- This is pretty crap, but appears the only way to avoid vivado issues

  process(clk_fr)
  begin
    if rising_edge(clk_fr) then
      dc <= not dc;
    end if;
  end process;

  dc_buf : BUFG
    port map(
      i => dc,
      o => clk_dc
      );

  phy : gig_eth_pcs_pma_basex_gtp
    port map(
      gtrefclk_p             => gt_clkp_i,
      gtrefclk_n             => gt_clkn_i,
      gtrefclk_out           => open,
      gtrefclk_bufg_out      => clk_fr,
      txp                    => gt_txp,
      txn                    => gt_txn,
      rxp                    => gt_rxp,
      rxn                    => gt_rxn,
      resetdone              => phy_done,
      userclk_out            => open,
      userclk2_out           => clk125,
      rxuserclk_out          => open,
      rxuserclk2_out         => open,
      pma_reset_out          => open,
      mmcm_locked_out        => mmcm_locked,
      independent_clock_bufg => clk_dc,
      gmii_txd               => gmii_txd,
      gmii_tx_en             => gmii_tx_en,
      gmii_tx_er             => gmii_tx_er,
      gmii_rxd               => gmii_rxd,
      gmii_rx_dv             => gmii_rx_dv,
      gmii_rx_er             => gmii_rx_er,
      gmii_isolate           => open,
      configuration_vector   => "00000",
      status_vector          => open,
      reset                  => rsti,
      signal_detect          => sig_det,
      gt0_pll0outclk_out     => pllclk_out,
      gt0_pll0outrefclk_out  => pllrefclk_out,
      gt0_pll1outclk_out     => open,
      gt0_pll1outrefclk_out  => open,
      gt0_pll0refclklost_out => open,
      gt0_pll0lock_out       => open
      );

  sig_det <= not sfp_los;

end rtl;
