library ipbus;

use work.DAQ_CTRL_Ctrl.all;
use work.FW_INFO_Ctrl.all;
use work.TDC_Ctrl.all;
use work.ipbus.all;
use work.ipbus_decode_registers.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

entity control is
  port(

    reset : in std_logic;
    clock : in std_logic;

    fw_info_mon : in FW_INFO_Mon_t;

    tdc_mon  : in  TDC_Mon_t;
    tdc_ctrl : out TDC_Ctrl_t;

    daq_ctrl_mon  : in  DAQ_CTRL_Mon_t;
    daq_ctrl_ctrl : out DAQ_CTRL_Ctrl_t;

    ipb_w : in  ipb_wbus;
    ipb_r : out ipb_rbus;

    -- fifo
    daq_rd_en : out std_logic;
    daq_din   : in  std_logic_vector(31 downto 0);
    daq_valid : in  std_logic;
    daq_empty : in  std_logic

    );
end control;

architecture behavioral of control is

  signal loopback : std_logic_vector (31 downto 0) := x"01234567";

  signal ipb_w_array : ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_r_array : ipb_rbus_array(N_SLAVES - 1 downto 0);

begin

  -- Special Loopback register at Adr 0...
  -- doesn't depend on regmap infrastructure etc.
  process (clock) is
  begin
    if (rising_edge(clock)) then
      ipb_r_array(N_SLV_LOOPBACK).ipb_ack <= '0';
      ipb_r_array(N_SLV_LOOPBACK).ipb_err <= '0';
      if (ipb_w_array(N_SLV_LOOPBACK).ipb_strobe) then
        ipb_r_array(N_SLV_LOOPBACK).ipb_ack <= '1';
        if (ipb_w_array(N_SLV_LOOPBACK).ipb_write) then
          loopback <= ipb_w_array(N_SLV_LOOPBACK).ipb_wdata;
        else
          ipb_r_array(N_SLV_LOOPBACK).ipb_rdata <= loopback;
        end if;
      end if;
    end if;
  end process;

  --------------------------------------------------------------------------------
  -- ipbus fabric selector
  --------------------------------------------------------------------------------

  fabric : entity work.ipbus_fabric_sel
    generic map(
      NSLV      => N_SLAVES,
      SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in          => ipb_w,
      ipb_out         => ipb_r,
      sel             => ipbus_sel_registers(ipb_w.ipb_addr),
      ipb_to_slaves   => ipb_w_array,
      ipb_from_slaves => ipb_r_array
      );

  --------------------------------------------------------------------------------
  -- TDC Interface
  --------------------------------------------------------------------------------

  TDC_wb_interface : entity work.TDC_wb_interface
    port map (
      clk       => clock,
      reset     => reset,
      wb_addr   => ipb_w_array(N_SLV_TDC).ipb_addr,
      wb_wdata  => ipb_w_array(N_SLV_TDC).ipb_wdata,
      wb_strobe => ipb_w_array(N_SLV_TDC).ipb_strobe,
      wb_write  => ipb_w_array(N_SLV_TDC).ipb_write,
      wb_rdata  => ipb_r_array(N_SLV_TDC).ipb_rdata,
      wb_ack    => ipb_r_array(N_SLV_TDC).ipb_ack,
      wb_err    => ipb_r_array(N_SLV_TDC).ipb_err,
      mon       => tdc_mon,
      ctrl      => tdc_ctrl
      );

  --------------------------------------------------------------------------------
  -- FW Info
  --------------------------------------------------------------------------------

  FW_INFO_wb_interface : entity work.FW_INFO_wb_interface
    port map (
      clk       => clock,
      reset     => reset,
      wb_addr   => ipb_w_array(N_SLV_FW_INFO).ipb_addr,
      wb_wdata  => ipb_w_array(N_SLV_FW_INFO).ipb_wdata,
      wb_strobe => ipb_w_array(N_SLV_FW_INFO).ipb_strobe,
      wb_write  => ipb_w_array(N_SLV_FW_INFO).ipb_write,
      wb_rdata  => ipb_r_array(N_SLV_FW_INFO).ipb_rdata,
      wb_ack    => ipb_r_array(N_SLV_FW_INFO).ipb_ack,
      wb_err    => ipb_r_array(N_SLV_FW_INFO).ipb_err,
      mon       => fw_info_mon
      );

  --------------------------------------------------------------------------------
  -- Wishbone FIFO Reader
  --------------------------------------------------------------------------------

  wishbone_fifo_reader_inst : entity work.wishbone_fifo_reader
    port map (
      clk       => clock,
      reset     => reset,
      ipbus_in  => ipb_w_array(N_SLV_DAQ_FIFO),
      ipbus_out => ipb_r_array(N_SLV_DAQ_FIFO),
      rd_en     => daq_rd_en,
      din       => daq_din,
      valid     => daq_valid,
      empty     => daq_empty
      );

  --------------------------------------------------------------------------------
  -- DAQ Control
  --------------------------------------------------------------------------------

  DAQ_CTRL_wb_interface : entity work.DAQ_CTRL_wb_interface
    port map (
      clk       => clock,
      reset     => reset,
      wb_addr   => ipb_w_array(N_SLV_DAQ_CTRL).ipb_addr,
      wb_wdata  => ipb_w_array(N_SLV_DAQ_CTRL).ipb_wdata,
      wb_strobe => ipb_w_array(N_SLV_DAQ_CTRL).ipb_strobe,
      wb_write  => ipb_w_array(N_SLV_DAQ_CTRL).ipb_write,
      wb_rdata  => ipb_r_array(N_SLV_DAQ_CTRL).ipb_rdata,
      wb_ack    => ipb_r_array(N_SLV_DAQ_CTRL).ipb_ack,
      wb_err    => ipb_r_array(N_SLV_DAQ_CTRL).ipb_err,
      ctrl      => daq_ctrl_ctrl,
      mon       => daq_ctrl_mon
      );
end behavioral;
