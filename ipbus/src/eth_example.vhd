library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.FW_INFO_Ctrl.all;
use work.TDC_Ctrl.all;
use work.DAQ_CTRL_Ctrl.all;

use work.ipbus.all;

entity eth_example is
  generic (
    -- these generics get set by hog at synthesis
    GLOBAL_DATE : std_logic_vector (31 downto 0) := x"DEFFFFFF";
    GLOBAL_TIME : std_logic_vector (31 downto 0) := x"DEFFFFFF";
    GLOBAL_VER  : std_logic_vector (31 downto 0) := x"DEFFFFFF";
    GLOBAL_SHA  : std_logic_vector (31 downto 0) := x"DEFFFFFF"
    );
  port(
    eth_clk_p : in  std_logic;          -- 125MHz MGT clock
    eth_clk_n : in  std_logic;
    eth_rx_p  : in  std_logic;          -- Ethernet MGT input
    eth_rx_n  : in  std_logic;
    eth_tx_p  : out std_logic;          -- Ethernet MGT output
    eth_tx_n  : out std_logic;
    eth_los   : in  std_logic;
    eth_txdis : out std_logic;

    en_gtpwr : out std_logic;

    led  : out std_logic;
    rled : out std_logic;
    gled : out std_logic
    );

end eth_example;

architecture rtl of eth_example is

  constant DAQ_FIFO_DEPTH : positive := 32768;
  constant DAQ_FIFO_WORDCNT_WIDTH : positive := integer(ceil(log2(real(DAQ_FIFO_DEPTH))));
  signal daq_wr_data_count : std_logic_vector (DAQ_FIFO_WORDCNT_WIDTH-1 downto 0) := (others => '0');

  component x_oneshot
    generic (DEADTIME : integer);
    port (
      d       : in  std_logic;
      clk     : in  std_logic;
      slowclk : in  std_logic;
      q       : out std_logic
      );
  end component;

  component x_flashsm
    generic (MXCNT : integer);
    port (
      trigger : in  std_logic;
      hold    : in  std_logic;
      clock   : in  std_logic;
      out_o   : out std_logic
      );
  end component;

  component device_dna
    port (
      clock : in  std_logic;
      reset : in  std_logic;
      dna   : out std_logic_vector (56 downto 0)
      );
  end component;

  signal clk_ipb, clk_125, rst_ipb, clk_aux, rst_aux, nuke, soft_rst : std_logic;

  signal onehz       : std_logic;
  signal mac_rx_last : std_logic;

  signal ipb_debug : std_logic_vector (3 downto 0);

  type ip_addr_t is array (integer range 3 downto 0) of integer range 0 to 255;
  signal MAC_ADDR    : std_logic_vector (47 downto 0) := x"00_50_51_fe_00_00";
  signal IP_ADDR     : ip_addr_t                      := (192, 168, 0, 10);
  signal ip_addr_slv : std_logic_vector(31 downto 0);

  function to_slv (addr : ip_addr_t) return std_logic_vector is
    variable slv : std_logic_vector(31 downto 0);
  begin
    slv(31 downto 24) := std_logic_vector(to_unsigned(addr(3), 8));
    slv(23 downto 16) := std_logic_vector(to_unsigned(addr(2), 8));
    slv(15 downto 8)  := std_logic_vector(to_unsigned(addr(1), 8));
    slv(7 downto 0)   := std_logic_vector(to_unsigned(addr(0), 8));
    return slv;
  end;

  signal ipb_w : ipb_wbus := (ipb_strobe => '0', ipb_addr => (others => '0'),
                              ipb_wdata  => (others => '0'), ipb_write => '0');
  signal ipb_r : ipb_rbus;

  signal rled_os, rled_flash : std_logic := '0';
  signal gled_os, gled_flash : std_logic := '0';

  signal dna        : std_logic_vector (56 downto 0);
  signal eth_locked : std_logic := '0';

  --------------------------------------------------------------------------------
  -- Ctrl & Monitoring
  --------------------------------------------------------------------------------

  signal daq_mon  : DAQ_CTRL_Mon_t;
  signal daq_ctrl : DAQ_CTRL_Ctrl_t;

  signal tdc_mon  : TDC_Mon_t;
  signal tdc_ctrl : TDC_Ctrl_t;

  signal fw_info_mon : FW_INFO_Mon_t;

  signal daq_wr_en        : std_logic;
  signal daq_rd_en        : std_logic;
  signal daq_din          : std_logic_vector(31 downto 0);
  signal daq_dout         : std_logic_vector(31 downto 0);
  signal daq_valid        : std_logic;
  signal daq_full         : std_logic;
  signal daq_overflow     : std_logic;
  signal daq_overflow_cnt : std_logic_vector(15 downto 0);
  signal daq_almost_full  : std_logic;
  signal daq_empty        : std_logic;

begin

  --------------------------------------------------------------------------------
  -- Ethernet Infrastructure
  --------------------------------------------------------------------------------

  eth_txdis <= '0';
  en_gtpwr  <= '1';

  -- get device unique id (DNA) and use it to set the MAC
  MAC_ADDR (15 downto 0) <= dna (15 downto 0);
  device_dna_inst : device_dna
    port map (
      clock => clk_ipb,
      reset => rst_ipb,
      dna   => dna
      );

  ip_addr_slv <= to_slv (ip_addr);

  te014_infra_inst : entity work.te014_infra
    port map (
      eth_clk_p     => eth_clk_p,
      eth_clk_n     => eth_clk_n,
      eth_rx_p      => eth_rx_p,
      eth_rx_n      => eth_rx_n,
      eth_tx_p      => eth_tx_p,
      eth_tx_n      => eth_tx_n,
      sfp_los       => eth_los,
      clk_ipb_o     => clk_ipb,
      rst_ipb_o     => rst_ipb,
      clk125_o      => clk_125,
      locked_o      => eth_locked,
      mac_rx_last_o => mac_rx_last,
      onehz_o       => onehz,
      rst125_o      => open,
      clk200        => open,            -- out
      nuke          => '0',
      soft_rst      => '0',
      leds          => open,
      debug         => ipb_debug,
      mac_addr      => mac_addr,
      ip_addr       => ip_addr_slv,
      ipb_in        => ipb_r,
      ipb_out       => ipb_w
      );

  --------------------------------------------------------------------------------
  -- Wishbone Control
  --------------------------------------------------------------------------------

  control_inst : entity work.control
    port map (
      reset => rst_ipb,
      clock => clk_ipb,

      ipb_w => ipb_w,
      ipb_r => ipb_r,

      -- control and monitoring
      fw_info_mon => fw_info_mon,

      tdc_mon  => tdc_mon,
      tdc_ctrl => tdc_ctrl,

      daq_ctrl_mon  => daq_mon,
      daq_ctrl_ctrl => daq_ctrl,

      -- daq
      daq_rd_en => daq_rd_en,
      daq_din   => daq_dout,
      daq_valid => daq_valid,
      daq_empty => daq_empty
      );

  fw_info_mon.HOG_INFO.GLOBAL_DATE <= GLOBAL_DATE;
  fw_info_mon.HOG_INFO.GLOBAL_TIME <= GLOBAL_TIME;
  fw_info_mon.HOG_INFO.GLOBAL_VER  <= GLOBAL_VER;
  fw_info_mon.HOG_INFO.GLOBAL_SHA  <= GLOBAL_SHA;

  --------------------------------------------------------------------------------
  -- LED Logic
  --------------------------------------------------------------------------------

  x_oneshot_rled : x_oneshot
    generic map (DEADTIME => 2**21-1)
    port map (
      d       => ipb_w.ipb_strobe,
      clk     => clk_ipb,
      slowclk => clk_ipb,
      q       => rled_os
      );

  x_flashsm_rled : x_flashsm
    generic map (MXCNT => 20)
    port map (
      trigger => rled_os,
      hold    => '0',
      clock   => clk_ipb,
      out_o   => rled_flash
      );

  x_oneshot_gled : x_oneshot
    generic map (DEADTIME => 2**21-1)
    port map (
      d       => mac_rx_last,
      clk     => clk_ipb,
      slowclk => clk_ipb,
      q       => gled_os
      );

  x_flashsm_gled : x_flashsm
    generic map (MXCNT => 20)
    port map (
      trigger => gled_os,
      hold    => '0',
      clock   => clk_ipb,
      out_o   => gled_flash
      );

  rled <= eth_locked xor rled_flash;
  gled <= eth_locked xor gled_flash;

  led <= onehz;                         -- heartbeat

  daq_mon.full  <= daq_full;
  daq_mon.empty <= daq_empty;
  daq_mon.overflow_cnt <= daq_overflow_cnt;

  counter_inst : entity work.counter
    generic map (
      g_COUNTER_WIDTH => 16
      )
    port map (
      clock   => clk_ipb,
      reset_i => rst_ipb,
      en_i    => daq_overflow,
      count_o => daq_overflow_cnt
      );

  daq_mon.word_cnt (DAQ_FIFO_WORDCNT_WIDTH-1 downto 0) <= daq_wr_data_count;

  fifo_sync_1 : entity work.fifo_sync
    generic map (
      DEPTH               => DAQ_FIFO_DEPTH,
      USE_ALMOST_FULL     => 1,
      WR_WIDTH            => 32,
      RD_WIDTH            => 32,
      WR_DATA_COUNT_WIDTH => DAQ_FIFO_WORDCNT_WIDTH,
      USE_WR_DATA_COUNT   => 1
      )
    port map (
      rst           => daq_ctrl.reset,  -- Must be synchronous to wr_clk. Must be applied only when wr_clk is stable and free-running.
      clk           => clk_ipb,
      wr_en         => daq_wr_en,
      rd_en         => daq_rd_en,
      din           => daq_din,
      dout          => daq_dout,
      valid         => daq_valid,
      wr_data_count => daq_wr_data_count,
      overflow      => daq_overflow,
      full          => daq_full,
      almost_full   => daq_almost_full,
      empty         => daq_empty
      );

  process (clk_ipb) is
  begin
    if (rising_edge(clk_ipb)) then

      if (daq_almost_full = '0') then
        daq_wr_en <= '1';
        if (daq_din = x"FFFFFFFF") then
          daq_din <= x"00000000";
        else
          daq_din <= std_logic_vector(to_unsigned(to_integer(unsigned(daq_din)) + 1, 32));
        end if;
      else
        daq_wr_en <= '0';
      end if;
    end if;
  end process;


end rtl;
