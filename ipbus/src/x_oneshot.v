`timescale 1ns / 1ps
//`define DEBUG_X_ONESHOT 1
//------------------------------------------------------------------------------------------------------------------
// Digital One-Shot:
//    Produces 1-clk wide pulse when d goes high.
//    Waits for d to go low before re-triggering.
//    Also has a programmable deadtime

module x_oneshot (d,clk,slowclk,q);

   parameter DEADTIME = 8;

   input     d;
   input     clk;
   input     slowclk;
   output    q;

   // State Machine declarations
   reg [2:0] sm;    // synthesis attribute safe_implementation of sm is "yes";
   parameter idle  =  0;
   parameter hold  =  1;

   localparam NBITS = $clog2(DEADTIME);
   reg [NBITS-1:0] halt = 'd0;

   always @(posedge slowclk) begin
      if (d && (sm==idle))
        halt <= DEADTIME-1'b1;
      else if (halt!=0)
        halt <= halt - 1'b1;
      else
        halt <= 0;
   end

   wire done = (halt==0);

   // One-shot state machine
   initial sm = idle;

   always @(posedge clk) begin
      case (sm)
        idle:    if (d)         sm <= hold;
        hold:    if(!d && done) sm <= idle;
        default:                sm <= idle;
      endcase
   end

   // Output FF
   reg  q = 0;

   generate
      always @(posedge clk) begin
         q <= d && (sm==idle);
      end
   endgenerate

   // Debug state machine display
`ifdef DEBUG_X_ONESHOT
   output [39:0] sm_dsp;
   reg [39:0]    sm_dsp;

   always @* begin
      case (sm)
        idle:   sm_dsp <= "idle ";
        hold:   sm_dsp <= "hold ";
        default sm_dsp <= "deflt";
      endcase
   end
`endif

//------------------------------------------------------------------------------------------------------------------
endmodule
//------------------------------------------------------------------------------------------------------------------
