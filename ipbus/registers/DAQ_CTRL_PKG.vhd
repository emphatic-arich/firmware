--This file was auto-generated.
--Modifications might be lost.
library IEEE;
use IEEE.std_logic_1164.all;


package DAQ_CTRL_CTRL is
  type DAQ_CTRL_MON_t is record
    WORD_CNT                   :std_logic_vector(31 downto 0);
    EMPTY                      :std_logic;                    
    FULL                       :std_logic;                    
    OVERFLOW_CNT               :std_logic_vector(15 downto 0);
  end record DAQ_CTRL_MON_t;


  type DAQ_CTRL_CTRL_t is record
    RESET                      :std_logic;   
  end record DAQ_CTRL_CTRL_t;


  constant DEFAULT_DAQ_CTRL_CTRL_t : DAQ_CTRL_CTRL_t := (
                                                         RESET => '0'
                                                        );


end package DAQ_CTRL_CTRL;