--This file was auto-generated.
--Modifications might be lost.
library IEEE;
use IEEE.std_logic_1164.all;


package TDC_CTRL is
  type TDC_MON_t is record
    READ_REG                   :std_logic_vector(31 downto 0);
  end record TDC_MON_t;


  type TDC_CTRL_t is record
    WRITE_REG                  :std_logic_vector(31 downto 0);
  end record TDC_CTRL_t;


  constant DEFAULT_TDC_CTRL_t : TDC_CTRL_t := (
                                               WRITE_REG => (others => '0')
                                              );


end package TDC_CTRL;