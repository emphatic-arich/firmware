-- write_uart.vhd:  UART receiver

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity write_uart is

  port (
    clk25    : in  std_logic;           -- 25MHz board clock
    TX       : out std_logic;
    tx_data  : in  std_logic_vector(7 downto 0);
    tx_valid : in  std_logic
    );

end entity write_uart;


architecture arch of write_uart is

  component baud is
    port (
      clk25   : in  std_logic;
      baud_en : out std_logic);
  end component baud;

  component uart_tx6 is
    port (
      data_in             : in  std_logic_vector(7 downto 0);
      en_16_x_baud        : in  std_logic;
      serial_out          : out std_logic;
      buffer_write        : in  std_logic;
      buffer_data_present : out std_logic;
      buffer_half_full    : out std_logic;
      buffer_full         : out std_logic;
      buffer_reset        : in  std_logic;
      clk                 : in  std_logic);
  end component uart_tx6;

  signal en_16_x_baud : std_logic;

begin

  uart_tx6_1 : entity work.uart_tx6
    port map (
      data_in             => tx_data,
      en_16_x_baud        => en_16_x_baud,
      serial_out          => TX,
      buffer_write        => tx_valid,
      buffer_data_present => open,
      buffer_half_full    => open,
      buffer_full         => open,
      buffer_reset        => '0',
      clk                 => clk25);

  baud_1 : entity work.baud
    port map (
      clk25   => clk25,
      baud_en => en_16_x_baud);

end architecture arch;
