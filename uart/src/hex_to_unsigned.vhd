--
-- hex_to_unsigned.vhd:  convert ascii hex to unsigned
-- set valid=1 if valid hex
-- bin is output binary 0-f
--
-- Note:  entirely asynchronous logic, user beware!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity hex_to_unsigned is

  port (
    hex : in std_logic_vector(7 downto 0);
    valid : out std_logic;
    bin : out unsigned(3 downto 0));
    
end entity hex_to_unsigned;

architecture synth of hex_to_unsigned is

  signal res_s : unsigned(7 downto 0);

begin  -- architecture synth

  hexi: process (hex) is
  begin  -- process hexi
    valid <= '0';
    -- handle digits 0-9
    if unsigned(hex) > X"2F" and unsigned(hex) < X"3A" then
      valid <= '1';
      res_s <= unsigned(hex) - X"30";
    -- handle uppercase A-F
    elsif unsigned(hex) > X"40" and unsigned(hex) < X"47" then
      valid <= '1';
      res_s <= unsigned(hex) - X"41" + 10;
    -- handle lowercase a-f
    elsif unsigned(hex) > X"60" and unsigned(hex) < X"67" then
      valid <= '1';
      res_s <= unsigned(hex) - X"61" + 10;
    end if;
    bin <= res_s(3 downto 0);
  end process hexi;
end architecture synth;
