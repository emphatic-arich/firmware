-- baud.vhd:  UART receiver

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity baud is

  port (
    clk25    : in  std_logic;           -- 25MHz board clock
    baud_en : out std_logic);

end entity baud;


architecture arch of baud is

--
-- Signals used to define baud rate
--
  signal baud_rate_counter : unsigned(7 downto 0) := "00000000";
  signal en_16_x_baud      : std_logic            := '0';

begin

  baud_en <= en_16_x_baud;

-- generate 9600 baud from 25Mhz
  baud_rate : process(clk25)
  begin
    if rising_edge(clk25) then
      if baud_rate_counter = 162 then
        baud_rate_counter <= "00000000";
        en_16_x_baud      <= '1';       -- single cycle enable pulse
      else
        baud_rate_counter <= baud_rate_counter + 1;
        en_16_x_baud      <= '0';
      end if;
    end if;
  end process baud_rate;


end architecture arch;
