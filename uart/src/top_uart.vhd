-- top_uart.vhd
--
-- top-level Hog design for simple UART readout interface
--


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity top_uart is

  port (
    clk25 : in  std_logic;                     -- 25MHz board clock
    LED   : out std_logic_vector(2 downto 0);  -- 3 LEDs
--    A     : in  std_logic_vector(1 downto 0);  -- J17 pins 3,4
--    B     : out std_logic_vector(7 downto 0);  -- J17 pins 7..14
--    C     : in  std_logic_vector(7 downto 0);  -- J17 pins 15..22
    TX    : out std_logic;
    RX    : in  std_logic
    );

end entity top_uart;


architecture arch of top_uart is

  component read_uart is
    port (
      clk25    : in  std_logic;
      RX       : in  std_logic;
      rd_data  : out std_logic_vector(7 downto 0);
      rd_valid : out std_logic);
  end component read_uart;

  component write_uart is
    port (
      clk25    : in  std_logic;
      TX       : out std_logic;
      tx_data  : in  std_logic_vector(7 downto 0);
      tx_valid : in  std_logic);
  end component write_uart;

  signal rd_data  : std_logic_vector(7 downto 0);
  signal rd_valid : std_logic;

  signal wr_data  : std_logic_vector(7 downto 0);
  signal wr_valid : std_logic;

begin

  read_uart_1 : entity work.read_uart
    port map (
      clk25    => clk25,
      RX       => RX,
      rd_data  => rd_data,
      rd_valid => rd_valid);

  write_uart_1 : entity work.write_uart
    port map (
      clk25    => clk25,
      TX       => TX,
      tx_data  => wr_data,
      tx_valid => wr_valid);

  wr_data  <= rd_data;
  wr_valid <= rd_valid;

  process (clk25) is
  begin  -- process
    if rising_edge(clk25) then          -- rising clock edge

      if rd_valid = '1' then
        LED(0) <= rd_data(0);
      end if;

    end if;
  end process;

end architecture arch;
