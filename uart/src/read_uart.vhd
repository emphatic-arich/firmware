-- read_uart.vhd:  UART receiver

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity read_uart is

  port (
    clk25    : in  std_logic;           -- 25MHz board clock
    RX       : in  std_logic;
    rd_data  : out std_logic_vector(7 downto 0);
    rd_valid : out std_logic
    );

end entity read_uart;


architecture arch of read_uart is

  component uart_rx6 is
    port (
      serial_in           : in  std_logic;
      en_16_x_baud        : in  std_logic;
      data_out            : out std_logic_vector(7 downto 0);
      buffer_read         : in  std_logic;
      buffer_data_present : out std_logic;
      buffer_half_full    : out std_logic;
      buffer_full         : out std_logic;
      buffer_reset        : in  std_logic;
      clk                 : in  std_logic);
  end component uart_rx6;

  signal rx_s : std_logic;

--
-- Signals used to connect UART_RX6
--
  signal uart_rx_data_out     : std_logic_vector(7 downto 0);
  signal read_from_uart_rx    : std_logic := '0';
  signal uart_rx_data_present : std_logic;
  signal uart_rx_half_full    : std_logic;
  signal uart_rx_full         : std_logic;
  signal uart_rx_reset        : std_logic;

--
-- Signals used to define baud rate
--
  signal baud_rate_counter : unsigned(7 downto 0) := "00000000";
  signal en_16_x_baud      : std_logic            := '0';

  signal rx_cmd              : std_logic_vector(7 downto 0);
  signal rx_valid, rx_valid0 : std_logic;

begin

  -- wire I/Os
  rx_s <= RX;

  uart_rx_reset <= '0';

  uart_rx6_1 : entity work.uart_rx6
    port map (
      serial_in           => rx_s,
      en_16_x_baud        => en_16_x_baud,
      data_out            => uart_rx_data_out,
      buffer_read         => read_from_uart_rx,
      buffer_data_present => uart_rx_data_present,
      buffer_half_full    => uart_rx_half_full,
      buffer_full         => uart_rx_full,
      buffer_reset        => uart_rx_reset,
      clk                 => clk25);

  -- a bit of combinatorial logic recommended by Ken Chapman
  read_from_uart_rx <= rx_valid and uart_rx_data_present;

  rxdat : process(clk25)
  begin
    if rising_edge(clk25) then

      rx_valid  <= '0';
      rx_valid0 <= rx_valid;

      -- edge detect to send only single valid pulse out
      rd_valid <= rx_valid and not rx_valid0;

      if uart_rx_data_present then
        -- capture only upper-case ASCII letters in hex range 40-5F
        -- LSB to LED, echo back
        rx_valid <= '1';
        rd_data  <= uart_rx_data_out;
      end if;
    end if;

  end process rxdat;


-- generate 9600 baud from 25Mhz
  baud_rate : process(clk25)
  begin
    if rising_edge(clk25) then
      if baud_rate_counter = 162 then
        baud_rate_counter <= "00000000";
        en_16_x_baud      <= '1';       -- single cycle enable pulse
      else
        baud_rate_counter <= baud_rate_counter + 1;
        en_16_x_baud      <= '0';
      end if;
    end if;
  end process baud_rate;


end architecture arch;
