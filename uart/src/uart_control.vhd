
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

--
-- simple UART-based control block
-- process the following commands:
--   "+xx"        set hex address xx
--   ">xx"        write hex value xx to address
--   "<"          read one hex byte from address
-- All other characters are ignored.  Hex values may use either
-- upper or lowercase letters.  Two digits are always required.
--
-- Address is latched on "+" command
-- wr_en strobed for one clock on write with data, address stable
-- rd_en taken high for one clock to request read, data must be stable
-- on subsequent rising clock edge
-- (or read data may be presented anytime and rd_en ignored)
--
-- for initial testing just R/W to 256-byte local RAM
--

entity uart_control is

  port (
    clk     : in  std_logic;
    rx      : in  std_logic;
    tx      : out std_logic;
    wr_en   : out  std_logic;
    rd_en   : out  std_logic;
    adr     : out  std_logic_vector(15 downto 0);
    wr_data : out  std_logic_vector(7 downto 0);
    rd_data : in std_logic_vector(7 downto 0));

end entity uart_control;

architecture synth of uart_control is

  component read_uart is
    port (
      clk25    : in  std_logic;
      RX       : in  std_logic;
      rd_data  : out std_logic_vector(7 downto 0);
      rd_valid : out std_logic);
  end component read_uart;

  component write_uart is
    port (
      clk25    : in  std_logic;
      TX       : out std_logic;
      tx_data  : in  std_logic_vector(7 downto 0);
      tx_valid : in  std_logic);
  end component write_uart;

  component hex_to_unsigned is
    port (
      hex   : in  std_logic_vector(7 downto 0);
      valid : out std_logic;
      bin   : out unsigned(3 downto 0));
  end component hex_to_unsigned;

  signal uart_rx_data : std_logic_vector(7 downto 0);
  signal uart_rx_valid : std_logic;

  signal uart_tx_data : std_logic_vector(7 downto 0);
  signal uart_tx_valid : std_logic;

  signal addr_s : std_logic_vector(15 downto 0);
  signal rd_data_s : std_logic_vector(7 downto 0);
  signal wr_data_s : std_logic_vector(7 downto 0);

  type t_regs is array(0 to 255) of std_logic_vector(7 downto 0);
  signal regs : t_regs;

  type state_type is ( sIDLE, sADDRH, sADDRL, sWRITEH, sWRITEL,
                       sWRITEIT, sREADIT, 
                       sREADH, sREADL );
  signal state : state_type;


  ----------------------------------------------
  ---- ugly thing I found on the internet
  ---- convert ascii hex to binary with no error check
  ----------------------------------------------
  function hex_to_bin(
    x : in std_logic_vector(7 downto 0))
    return std_logic_vector is
    variable v : std_logic_vector(3 downto 0);
  begin
    if (x = X"41" or x = X"61") then
      v := X"A";
    elsif (x = X"42" or x = X"62") then
      v := X"B";
    elsif (x = X"43" or x = X"63") then
      v := X"C";
    elsif (x = X"44" or x = X"64") then
      v := X"D";
    elsif (x = X"45" or x = X"65") then
      v := X"E";
    elsif (x = X"46" or x = X"66") then
      v := X"F";
    else
      v := x(3 downto 0);
    end if;
    return std_logic_vector(v);
  end;

  ---- ugly thing I wrote to convert binary to hex
  function bin_to_hex(
    b : in std_logic_vector(3 downto 0))
    return std_logic_vector is
    variable h : std_logic_vector(7 downto 0);
  begin
    if unsigned(b) > 9 then
      h := std_logic_vector( unsigned( b) + X"30");
    else
      h := std_logic_vector( unsigned( b) + X"37");
    end if;
    return std_logic_vector( h);
  end;
  


begin  -- architecture synth

  read_uart_1: entity work.read_uart
    port map (
      clk25    => clk25,
      RX       => RX,
      rd_data  => uart_rx_data,
      rd_valid => uart_rx_valid);

  write_uart_1: entity work.write_uart
    port map (
      clk25    => clk25,
      TX       => TX,
      tx_data  => uart_tx_data,
      tx_valid => uart_tx_valid);

  process (clk25) is
  begin  -- process
    if rising_edge(clk25) then       -- rising clock edge

      uart_tx_valid <= '0';

      if uart_rx_valid = '1' then
        LED(0) <= uart_rx_data(0);

        case state is
          when sIDLE =>
            uart_tx_data <= uart_rx_data;
            uart_tx_valid <= '1';
            if uart_rx_data = X"2B" then
              state <= sADDRH;
            elsif uart_rx_data = X"3E" then
              state <= sWRITEH;
            elsif uart_rx_data = X"3C" then
              state <= sREADIT;
            end if;
          when sADDRH =>
            uart_tx_data <= uart_rx_data;
            uart_tx_valid <= '1';
            addr_s(7 downto 4) <= hex_to_bin( uart_rx_data);
            state <= sADDRL;
          when sADDRL =>
            uart_tx_data <= uart_rx_data;
            uart_tx_valid <= '1';
            addr_s(3 downto 0) <= hex_to_bin( uart_rx_data);
            state <= sIDLE;
          when sWRITEH =>
            uart_tx_data <= uart_rx_data;
            uart_tx_valid <= '1';
            wr_data_s(7 downto 4) <= hex_to_bin( uart_rx_data);
            state <= sWRITEL;
          when sWRITEL =>
            uart_tx_data <= uart_rx_data;
            uart_tx_valid <= '1';
            wr_data_s(3 downto 0) <= hex_to_bin( uart_rx_data);
            state <= sWRITEIT;
          when sWRITEIT =>
            regs( to_integer( unsigned( addr_s))) <= wr_data_s;
            state <= sIDLE;
          when sREADIT =>
            rd_data_s <= regs( to_integer( unsigned( addr_s)));
            state <= sREADH;
          when sREADH =>
            uart_tx_data <= bin_to_hex( rd_data_s(7 downto 4));
            uart_tx_valid <= '1';
            state <= sREADL;
          when sREADL =>
            uart_tx_data <= bin_to_hex( rd_data_s(3 downto 0));
            uart_tx_valid <= '1';
            state <= sIDLE;
        end case;
        
      end if;
  
    end if;
  end process;

end architecture synth;
