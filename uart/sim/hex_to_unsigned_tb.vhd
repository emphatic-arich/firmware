--
-- hex_to_unsigned_tb.vhd:  test hex_to_unsigned
--


library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity hex_to_unsigned_tb is
end entity hex_to_unsigned_tb;

architecture bench of hex_to_unsigned_tb is

  component hex_to_unsigned is
    port (
      hex   : in  std_logic_vector(7 downto 0);
      valid : out std_logic;
      bin   : out unsigned(3 downto 0));
  end component hex_to_unsigned;

  signal hex_s : std_logic_vector(7 downto 0);
  signal valid_s : std_logic;
  signal bin_s : unsigned(3 downto 0);

begin

  hex_to_unsigned_1: entity work.hex_to_unsigned
    port map (
      hex   => hex_s,
      valid => valid_s,
      bin   => bin_s);

  stimulus : process
  begin

    hex_s <= X"2F";
    wait for 1 ns;

    hex_s <= X"30";
    wait for 1 ns;

    hex_s <= X"39";
    wait for 1 ns;

    hex_s <= X"3A";
    wait for 1 ns;

    hex_s <= X"40";
    wait for 1 ns;

    hex_s <= X"41";
    wait for 1 ns;

    hex_s <= X"46";
    wait for 1 ns;

    hex_s <= X"47";
    wait for 1 ns;

    hex_s <= X"60";
    wait for 1 ns;

    hex_s <= X"61";
    wait for 1 ns;

    hex_s <= X"66";
    wait for 1 ns;

    hex_s <= X"67";
    wait for 1 ns;



    wait;

  end process;


end;

