#!/usr/bin/env python3
#
# Handy list of reg methods:
#   reg.getAddress(
#   reg.getClient(
#   reg.getDescription(
#   reg.getFirmwareInfo(
#   reg.getId(
#   reg.getMask(
#   reg.getMode(
#   reg.getModule(
#   reg.getNode(
#   reg.getNodes(
#   reg.getParameters(
#   reg.getPath(
#   reg.getPermission(
#   reg.getSize(
#   reg.getTags(
#   reg.read(
#   reg.readBlock(
#   reg.readBlockOffset(
#   reg.write(
#   reg.writeBlock(
#   reg.writeBlockOffset(

import os
import random  # For randint
import uhal
import argparse
import sys
import time
from time import sleep

IPB_PATH = "ipbusudp-2.0://192.168.0.10:50001"
ADR_TABLE = "../address_tables/registers.xml"


def setup():
    uhal.disableLogging()
    hw = uhal.getDevice("my_device", IPB_PATH, "file://" + ADR_TABLE)
    return hw

def loopback_test(loops=10000000):

    hw = setup()

    start = time.time()

    id = "LOOPBACK.LOOPBACK"
    reg = hw.getNode(id)
    for i in range(loops):
        wr = random.randint(0, 2**32)
        reg.write(wr)
        rd = reg.read()
        hw.dispatch()

        if wr != rd:
            print("ERR: %d %s wr=0x%08X rd=0x%08X" % (i, id, wr, rd))
            return
        if (i % (loops/1000) == 0 and i != 0):
            timediff = time.time() - start
            Mb = i*32/1000000.0
            Mbps = Mb/timediff
            print("%i reads done... (%.2f Mb, %f Mb/s)" % (i, Mb, Mbps))

def write_node(id, value):
    hw = setup()
    reg = hw.getNode(id)
    if (reg.getPermission() == uhal.NodePermission.WRITE):
        action_reg(hw, reg)
    else:
        reg.write(value)
        hw.dispatch()


def read_node(id):
    hw = setup()
    reg = hw.getNode(id)
    ret = reg.read()
    hw.dispatch()
    return ret


def printregs():

    hw = setup()

    for id in hw.getNodes():
        reg = hw.getNode(id)
        # if (reg.getModule() == ""):
        if (reg.getMode() != uhal.BlockReadWriteMode.HIERARCHICAL):
            print(format_reg(reg.getAddress(), reg.getPath()[4:], -1,
                             format_permission(reg.getPermission())))


def action(id):
    hw = setup()
    reg = hw.getNode(id)
    addr = reg.getAddress()
    mask = reg.getMask()
    hw.getClient().write(addr, mask)
    hw.dispatch()


def action_reg(hw, reg):
    addr = reg.getAddress()
    mask = reg.getMask()
    hw.getClient().write(addr, mask)
    hw.dispatch()


def regdump():
    hw = setup()
    for id in hw.getNodes():
        reg = hw.getNode(id)
        if (((reg.getPermission() == uhal.NodePermission.READ) or
             (reg.getPermission() == uhal.NodePermission.READWRITE)) and
            (reg.getMode() != uhal.BlockReadWriteMode.HIERARCHICAL)):
            print_reg(hw, reg, "")


def print_reg(hw, reg, pad=""):
    val = reg.read()
    id = reg.getPath()
    hw.dispatch()
    print(format_reg(reg.getAddress(), id[4:], val,
                     format_permission(reg.getPermission())))


def tab_pad(s, maxlen):
    return (s+"\t"*(int((8*maxlen-len(s)-1)/8)+1))


def format_reg(address, name, val, permission=""):
    s = ("0x%04X" % address) + ' ' + tab_pad(permission, 1) \
        + tab_pad(str(name), 8)
    if (val != -1):
        s = s + ("0x%08X" % val)
    return s


def format_permission(perm):
    if perm == uhal.NodePermission.READ:
        return "r"
    if perm == uhal.NodePermission.READWRITE:
        return "rw"
    if perm == uhal.NodePermission.WRITE:
        return "w"


def read_daq(hw):

    loops = 1000
    block = 8096

    # data = hw.getNode ("DAQ_FIFO").readBlock (6)
    # hw.dispatch()
    # for i in data:
    #     print("%08X" % i)

    # SPEED TEST

    start = time.time()

    for i in range (loops):
         data = hw.getNode ("DAQ_FIFO").readBlock (block)
         hw.dispatch()

    timediff = time.time() - start

    speed = 32.0 * loops * block  / timediff / 1E6
    print("Speed = %f Mbps" % speed)

    # DATA INTEGRITY TEST

    loops = 1000

    last = 0
    for i in range (loops):

        data = hw.getNode ("DAQ_FIFO").readBlock (block)
        hw.dispatch()

        for reading in data:
            if (last != 0):
                assert last+1 == reading,  "last+1=%d , current=%d" % (last+1, reading)
            last = reading

        if (i % 100 == 0):
            print ("%f Mb with no errors..." % (i*block*32.0/1E6))

if __name__ == '__main__':
    #regdump()
    hw = setup()
    read_daq(hw)
    #read_daq(hw)
    #read_daq(hw)
    #loopback_test()

