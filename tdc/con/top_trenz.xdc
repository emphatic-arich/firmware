#
# Initial constraints file for Trenz module
#

# 25MHz crystal
set_property PACKAGE_PIN T14 [get_ports clk25]
set_property IOSTANDARD LVCMOS18 [get_ports clk25]
create_clock -period 40.000 -name sys_clk_pin -waveform {0.000 5.000} -add [get_ports clk25]

# LED on module
set_property PACKAGE_PIN K18 [get_ports {LED[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {LED[0]}]

# LEDs on carrier
set_property PACKAGE_PIN D11 [get_ports {LED[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[1]}]

set_property PACKAGE_PIN C12 [get_ports {LED[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[2]}]

## J17 pins
# 1, 2 - GND
# 3, 4 - port A[0-1]
# 5, 6 - 3.3V, V_CFG
# 7..14 - port B[0-7]
# 15..22 - port C[0-7]

#---- Port A (2 bits) ----
# Pin 3:  Net B14_L11_P
set_property PACKAGE_PIN P15 [get_ports {A[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {A[0]}]
set_property PULLUP true [get_ports {A[0]}]

# Pin 4:  Net B14_L11_N
set_property PACKAGE_PIN P16 [get_ports {A[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {A[1]}]
set_property PULLUP true [get_ports {A[1]}]

#---- Port B (8 bits) ----
# Pin 7:  Net B14_L22_P
set_property PACKAGE_PIN T12 [get_ports {B[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B[0]}]
set_property PULLUP true [get_ports {B[0]}]

# Pin 8:  Net B14_L22_N
set_property PACKAGE_PIN U12 [get_ports {B[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B[1]}]
set_property PULLUP true [get_ports {B[1]}]

# Pin 9:  Net B14_L7_P
set_property PACKAGE_PIN M16 [get_ports {B[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B[2]}]
set_property PULLUP true [get_ports {B[2]}]

# Pin 10: Net B14_L7_N
set_property PACKAGE_PIN M17 [get_ports {B[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B[3]}]
set_property PULLUP true [get_ports {B[3]}]

# Pin 11: Net B14_L19_P
set_property PACKAGE_PIN R13 [get_ports {B[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B[4]}]
set_property PULLUP true [get_ports {B[4]}]

# Pin 12: Net B14_L19_N
set_property PACKAGE_PIN T13 [get_ports {B[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B[5]}]
set_property PULLUP true [get_ports {B[5]}]

# Pin 13: Net B14_L13_N
set_property PACKAGE_PIN T15 [get_ports {B[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B[6]}]
set_property PULLUP true [get_ports {B[6]}]


# --- conflicts with CLK
# Pin 14: Net B14_L13_P
# set_property PACKAGE_PIN T14 [get_ports {B[7]}]
# Pin 29: Net B14_L10_P
set_property PACKAGE_PIN N18 [get_ports {B[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B[7]}]
set_property PULLUP true [get_ports {B[7]}]
# ---------------------

#---- port C ----
# Pin 15: Net B14_L14_N
set_property PACKAGE_PIN R17 [get_ports {C[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {C[0]}]
set_property PULLUP true [get_ports {C[0]}]

# Pin 16: Net B14_L14_P
set_property PACKAGE_PIN R16 [get_ports {C[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {C[1]}]
set_property PULLUP true [get_ports {C[1]}]

# Pin 17: Net B14_L5_N
set_property PACKAGE_PIN K15 [get_ports {C[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {C[2]}]
set_property PULLUP true [get_ports {C[2]}]

# Pin 18: Net B14_L5_P
#

set_property PACKAGE_PIN R18 [get_ports {C[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {C[7]}]
set_property PULLUP true [get_ports {C[7]}]

set_property PACKAGE_PIN T18 [get_ports {C[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {C[6]}]
set_property PULLUP true [get_ports {C[6]}]

set_property PACKAGE_PIN L18 [get_ports {C[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {C[5]}]
set_property PULLUP true [get_ports {C[5]}]

set_property PACKAGE_PIN K17 [get_ports {C[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {C[4]}]
set_property PULLUP true [get_ports {C[4]}]

set_property PACKAGE_PIN J14 [get_ports {C[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {C[3]}]
set_property PULLUP true [get_ports {C[3]}]

# J4 pins
# Pin 3:  B15_L11_p
set_property PACKAGE_PIN D13 [get_ports RX]
set_property IOSTANDARD LVCMOS33 [get_ports RX]
set_property PULLUP true [get_ports RX]

# Pin 4:  B15_L11_n
set_property PACKAGE_PIN C13 [get_ports TX]
set_property IOSTANDARD LVCMOS33 [get_ports TX]
