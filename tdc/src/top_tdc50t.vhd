-- top_tdc.vhd
--
-- temporary top-level design for Trenz 50T trial build
-- 96-channel inputs using 4 bits of C port to select a group of 8
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use work.tdc_types.all;

entity top_tdc50t is

  port (
    clk25 : in  std_logic;                     -- 25MHz board clock
    LED   : out std_logic_vector(2 downto 0);  -- 3 LEDs
    A     : in  std_logic_vector(1 downto 0);  -- J17 pins 3,4
    B     : in  std_logic_vector(7 downto 0);  -- J17 pins 7..14
    C     : in  std_logic_vector(7 downto 0)   -- J17 pins 15..22
    );

end entity top_tdc50t;


architecture arch of top_tdc50t is

  component top_tdc_logic is
    port (
      clk25   : in  std_logic;
      clk100  : out std_logic;          -- system 100MHz clock
      reset   : in  std_logic;
      trigger : in  std_logic;
      pulse   : in  std_logic_vector(NUM_TDC_CHANNELS-1 downto 0);
      daq     : out std_logic_vector(DAQ_OUT_BITS-1 downto 0);
      valid   : out std_logic);
  end component top_tdc_logic;

  component flash is
    generic (
      COUNTS : integer);
    port (
      clk100 : in  std_logic;
      trig   : in  std_logic;
      led    : out std_logic);
  end component flash;

  signal trigger_s, reset_s : std_logic;
  signal pulse_s            : std_logic_vector(NUM_TDC_CHANNELS-1 downto 0);
  signal daq_s              : std_logic_vector(DAQ_OUT_BITS-1 downto 0);
  signal valid_s            : std_logic;

  signal clk100 : std_logic;

  signal ctr : unsigned(31 downto 0);

  signal sel : std_logic_vector(3 downto 0);
  signal inp : std_logic_vector(7 downto 0);

begin

  reset_s   <= not A(0);
  trigger_s <= not A(1);

  sel <= C(3 downto 0);
  inp <= B;

  LED(0) <= xor_reduce( daq_s);
  LED(1) <= valid_s;
  LED(2) <= trigger_s;

  -- some funny business with inputs to trick the optimizer
  mux1: process (sel, pulse_s) is
  begin  -- process mux1

    case sel is
      when "0000" =>
        pulse_s <= X"1111" & X"2222" & X"3333" & X"4444" & X"5555" & inp & inp;
      when "0001" => 
        pulse_s <= X"1111" & X"2222" & X"3333" & X"4444" & inp & inp & X"6666";
      when "0010" => 
        pulse_s <= X"1111" & X"2222" & X"3333" & inp & inp & X"5555" & X"6666";
      when "0011" =>
        pulse_s <= X"1111" & X"2222" & inp & inp & X"4444" & X"5555" & X"6666";
      when "0100" => 
        pulse_s <= X"1111" & inp & inp & X"3333" & X"4444" & X"5555" & X"6666";
      when "0101" => 
        pulse_s <= inp & inp & X"2222" & X"3333" & X"4444" & X"5555" & X"6666";
      when "0110" => 
        pulse_s <= inp & inp & X"2222" & X"3333" & X"4444" & X"5555" & inp & inp;
      when "0111" => 
        pulse_s <= X"1111" & inp & inp & X"3333" & X"4444" & X"5555" & inp & inp;
      when "1000" => 
        pulse_s <= X"1111" & X"2222" & inp & inp & X"4444" & X"5555" & inp & inp;
      when "1001" => 
        pulse_s <= X"1111" & X"2222" & X"3333" & inp & inp & X"5555" & inp & inp;
      when "1010" => 
        pulse_s <= X"1111" & X"2222" & X"3333" & X"4444" & inp & inp & inp & inp;
      when "1011" => 
        pulse_s <= inp & inp & inp & inp & inp & inp & inp & inp & inp & inp & inp & inp;
      when others => null;
    end case;
    
  end process mux1;

  top_tdc_logic_1 : entity work.top_tdc_logic
    port map (
      clk25   => clk25,
      clk100  => clk100,
      reset   => reset_s,
      trigger => trigger_s,
      pulse   => pulse_s,
      daq     => daq_s,
      valid   => valid_s);

end architecture arch;
