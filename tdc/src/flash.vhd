------------------------------------------------------------------------
-- flash.vhd
--
-- Simple LED blinker
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity flash is
  
  generic (
    COUNTS : integer := 10000000);

  port (
    clk100 : in  std_logic;
    trig   : in  std_logic;
    led    : out std_logic);

end entity flash;


architecture synth of flash is

  signal ctr : unsigned( 23 downto 0);
  signal trig0 : std_logic;
  
begin  -- architecture synth

  process (clk100) is
  begin  -- process
    if rising_edge(clk100) then      -- rising clock edge
      trig0 <= trig;

      led <= '0';

      if trig = '1' and trig0 = '0' then
        ctr <= to_unsigned(COUNTS, ctr'length);
      else
        if ctr /= 0 then
          led <= '1';
          ctr <= ctr - 1;
        end if;
      end if;

    end if;
  end process;

end architecture synth;
