# Arich FPGA Firmware

FPGA firmware for the Emphatic ARICH SiPM TDC. 

[[_TOC_]]

## Useful Links
- [Trenz Schematic](http://gauss.bu.edu/svn/emphatic-doco/Docs/Trenz/SCH-TE0714-03-50-2IAC6.PDF)
- [Trenz Breakout Schematic](http://gauss.bu.edu/svn/emphatic-doco/Docs/Trenz/SCH-TEBB0714-01.PDF)
- [Redmine Documentation](https://gauss.bu.edu/redmine/projects/emphatic-doco)

This directory contains the source for the EMPHATIC TDC.
1-ns resolution TDC with 96 channels, Ethernet readout.

Target is XC7A50T.  Essential specs:

```
  52.1k LCELLS  65.2k FFs  150 18k BRAMs  4 MGTs
  Very roughly 50% LUTs and FFs used for bare 96ch TDC
```

## Readout

For now, a big FIFO.  Ideally burst or block read.
FIFO can and should have a word count register, or gracefully
handle block read of empty FIFO.

## Control

### Per TDC

* Required
  * Enable bit
* Optional/desired:
  * Rate meter
  * Trigger match counter
  * Width histogram

### Overall

* Global reset
* Trigger enable
* Trigger rate limit
* Minimum trigger spacing
* Total trigger count

### Fake data generator

* Triggers
  * Random trigger (obey minimum spacing, rate limit)
  * Periodic trigger
* Hits
  * Random hits (minimum spacing)

## Ethernet Interface

An IPBus Ethernet interface is provided.

The top-level address table is specified in `address_tables/registers.xml`,
each node in which corresponds to an independent Wishbone slave.

### Control and Monitoring Records

Hierarchical VHDL records are automatically generated to reflect
the hierarchy of specified Wishbone slaves.

#### Updating Wishbone Slaves

To **update** slaves (already connected to the Ctrl/Monitoring system):
1. Update the relevant XML module, e.g. `address_tables/modules/FW_INFO.xml`.
1. From the root directory of the repository, run `make reg`
1. The relevant entries should now be reflected in the Ctrl/Monitoring package,
   in this case located in `ipbus/registers/FW_INFO_PKG.vhd`.

#### Adding Wishbone Slaves

To **add new** Wishbone slaves (new Ctrl/Monitoring records):

1. Create an XML address table in `address_tables/modules/YOUR_MODULE.xml`
2. Update `address_tables/registers.xml` and add an entry for the new XML module
3. Create a symbolic link
   from `address_tables/modules/YOUR_MODULE.xml`
   to `ipbus/registers/YOUR_MODULE.xml`.
   This is used as a hint to the XML-->VHDL tool that this XML address table
   should be processed to generate VHDL and a Ctrl/Monitoring record
4. From the root directory of the repository, run `make reg`
5. In `ipbus/src/control.vhd`, copy and paste one of the existing slave
   instantiations and update the name:
   e.g. use the code snippet below and paste it at the end of the VHDL entity
``` vhdl
  YOUR_MODULE_wb_interface : entity work.YOUR_MODULE_wb_interface
    port map (
      clk       => clock,
      reset     => reset,
      wb_addr   => ipb_w_array(N_SLV_YOUR_MODULE).ipb_addr,
      wb_wdata  => ipb_w_array(N_SLV_YOUR_MODULE).ipb_wdata,
      wb_strobe => ipb_w_array(N_SLV_YOUR_MODULE).ipb_strobe,
      wb_write  => ipb_w_array(N_SLV_YOUR_MODULE).ipb_write,
      wb_rdata  => ipb_r_array(N_SLV_YOUR_MODULE).ipb_rdata,
      wb_ack    => ipb_r_array(N_SLV_YOUR_MODULE).ipb_ack,
      wb_err    => ipb_r_array(N_SLV_YOUR_MODULE).ipb_err,
      mon       => tdc_mon,
      ctrl      => tdc_ctrl
      );
```
6. Add the Ctrl/Monitoring records to the ports of this `control.vhd` module,
   and connect them to the relevant signals.
7. Add the relevant VHDL package to all the modules where you are using them, e.g.
``` vhdl
   use work.FW_INFO_Ctrl.all;
```

