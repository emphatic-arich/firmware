SHELL := /bin/bash

CCZE := $(shell command -v ccze 2> /dev/null)
ifndef CCZE
COLORIZE =
else
COLORIZE = | ccze -A
endif

IFTIME := $(shell command -v time 2> /dev/null)
ifndef IFTIME
TIMECMD =
else
TIMECMD = time -p
endif

export LD_LIBRARY_PATH=/opt/cactus/lib

list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

all: create synth impl

init:
	git submodule update --init --recursive

reg: decode
	rm -f ipbus/registers/*_PKG.vhd
	rm -f ipbus/registers/*_PKG.yml
	rm -f ipbus/registers/*_map.vhd
	cd ipbus/regmap && make

decode:
	/opt/cactus/bin/uhal/tools/gen_ipbus_addr_decode address_tables/registers.xml
	mv ipbus_decode_registers.vhd ipbus/registers/

create_eth_example:
	$(TIMECMD) Hog/CreateProject.sh eth_example $(COLORIZE)

synth_eth_example:
	$(TIMECMD) Hog/LaunchWorkflow.sh -synth_only eth_example $(COLORIZE)

impl_eth_example:
	$(TIMECMD) Hog/LaunchWorkflow.sh eth_example $(COLORIZE)

create_tdc:
	$(TIMECMD) Hog/CreateProject.sh tdc $(COLORIZE)

synth_tdc:
	$(TIMECMD) Hog/LaunchWorkflow.sh -synth_only tdc $(COLORIZE)

impl_tdc:
	$(TIMECMD) Hog/LaunchWorkflow.sh tdc $(COLORIZE)

clean:
	rm -rf Projects/
